# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  hsh = {}
  words = str.split(' ')
  words.each {|word| hsh[word] = word.length}
  hsh
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  max = 0
  name = ''
  hash.each do |key, val|
    if val > max
      max = val
      name = key
    end
  end
  name
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each do |key, val|
    older[key] = val
  end
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  hash = {}
  word.each_char do |letter|
    if hash[letter] == nil
      hash[letter] = 1
    else
      hash[letter] += 1
    end
  end
  hash
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  hash = {}
  arr.each do |ele|
    hash[ele] = 'wow'
  end
  hash.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  count = {'even' => 0, 'odd' => 0}
  numbers.each {|num| num.even? ? count['even'] += 1 : count['odd'] += 1}
  count
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  hash = {}
  string.each_char do |ch|
    if hash[ch] == nil
      hash[ch] = 1
    else
      hash[ch] += 1
    end
  end
  hash.sort_by {|key, val| val}.to_a[-1][0]
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  result = []
  students.each do |name, mnth|
    if mnth >= 7
      students.each do |name2, mnth2|
        if name2 != name && mnth2 >= 7 && !result.include?([name, name2]) && !result.include?([name2, name])
          result << [name, name2]
        end
      end
    end
  end
  result
end


# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  number_of_species = specimens.uniq.length
  smallest_population_size = specimens.count(specimens.min)
  largest_population_size = specimens.count(specimens.max)

  number_of_species**2 * smallest_population_size / largest_population_size
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  vand = vandalized_sign.delete("'.,;:!?/").downcase.split('').uniq
  vand.each do |ch|
    return false if vandalized_sign.count(ch) > normal_sign.count(ch)
  end
  true
end


def character_count(str)
end
